package controlador;

import modelo.Producto;
import util.Util;

import java.security.spec.ECField;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CrudSqlite {
    public static boolean insert(){
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement = connection.createStatement();
            String querySql = Util.insert;
            statement.execute(querySql);
            statement.close();
            connection.close();
            System.out.println("Se realizo el insert");
            return true;
        }catch (Exception e){
            System.err.println("Fallo el insert");
            return false;
        }
    }
    public static boolean update(){
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement = connection.createStatement();
            String querySql = Util.updateById;
            statement.execute(querySql);
            statement.close();
            connection.close();
            System.out.println("Se realizo el update");
            return true;
        }catch (Exception e){
            System.err.println("Fallo el update");
            return false;
        }
    }

    public static boolean delete(String id){
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement = connection.createStatement();
            String querySql = Util.deleteById + id;
            statement.execute(querySql);
            statement.close();
            connection.close();
            System.out.println("Se realizo el delete");
            return true;
        }catch (Exception e){
            System.err.println("Fallo el delete");
            return false;
        }
    }
    public static List<Producto> select(){
        List<Producto> listProductos = new LinkedList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement = connection.createStatement();
            String querySql = Util.selectAllQuery;
            ResultSet resultSet = statement.executeQuery(querySql);
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String nombre = resultSet.getString("nombre");
                String categoria = resultSet.getString("categoria");
                double precio = resultSet.getDouble("precio");
                int cantidad = resultSet.getInt("cantidad");
                listProductos.add(new Producto(nombre,categoria,precio,id,cantidad));
            }
            statement.close();
            connection.close();
            System.out.println("Se realizo el select");
            return listProductos;
        }catch (Exception e){
            System.err.println("Fallo el select "+e.getMessage());
            return listProductos;
        }
    }

    public static Producto selectById(String id){
        try {
            Producto producto = null;
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement = connection.createStatement();
            String querySql = Util.selectById + id;
            ResultSet resultSet = statement.executeQuery(querySql);
            while (resultSet.next()){
                int idProduc = resultSet.getInt("id");
                String nombre = resultSet.getString("nombre");
                String categoria = resultSet.getString("categoria");
                double precio = resultSet.getDouble("precio");
                int cantidad = resultSet.getInt("cantidad");
                producto = new Producto(nombre,categoria,precio,idProduc,cantidad);
            }
            statement.close();
            connection.close();
            System.out.println("Se realizo el select");
            return producto;
        }catch (Exception e){
            System.err.println("Fallo el select "+e.getMessage());
            return null;
        }
    }

    public static boolean crearTabla(){
        try{
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Farmacia.sqlite");
            Statement statement= connection.createStatement();
            String querySql = Util.createTable;
            statement.execute(querySql);
            statement.close();
            connection.close();
            System.out.println("Tabla fue creada");
            return true;
        }catch (Exception e){
            System.out.println("Error al crear la tabla "+e.getMessage());
            return false;
        }
    }
}
