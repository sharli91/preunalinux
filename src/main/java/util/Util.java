package util;

public class Util {
    public static String selectAllQuery= "SELECT * FROM productos;";
    public static String selectById= "SELECT * FROM productos WHERE id = ";
    public static String deleteById= "DELETE FROM productos WHERE id = ";
    public static String updateById= "UPDATE productos SET nombre = 'loratadina' WHERE id = 1;";
    public static String insert= "INSERT INTO productos (nombre,cantidad,precio,categoria) VALUES ('insulina',20,50000,'refrigerado');";

    public static String createTable= "CREATE TABLE \"productos\" (\n" +
            "\t\"id\"\tINTEGER,\n" +
            "\t\"nombre\"\tTEXT NOT NULL,\n" +
            "\t\"cantidad\"\tINTEGER NOT NULL,\n" +
            "\t\"precio\"\tREAL NOT NULL,\n" +
            "\t\"categoria\"\tTEXT NOT NULL,\n" +
            "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
            ");";


}