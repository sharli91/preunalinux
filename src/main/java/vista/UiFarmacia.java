package vista;

import controlador.CrudSqlite;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UiFarmacia extends JFrame{
    private JTextField editId;
    private JPanel panelMain;
    private JButton btnEliminar;
    private JButton btnAgregar;


    public UiFarmacia() throws HeadlessException {
        this.setTitle("Farmacia");
        this.setSize(500,500);
        this.setContentPane(panelMain);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id = editId.getText();
                CrudSqlite.delete(id);
            }
        });

        btnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                CrudSqlite.insert();
            }
        });
    }

    public static void main(String[] args) {
        UiFarmacia ventana = new UiFarmacia();
    }
}
